const User = require('../models/User');
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth"); 

// Register a user

module.exports.registerUser = (pmBody) => {
	let newUser = new User({
		fullName: pmBody.fullName,
		email: pmBody.email,
		password: pmBody.password,
		isAdmin: pmBody.isAdmin
	})

	return newUser.save().then((createdUser, error) => {
		if(error) {
			return false
		} else	{
			return true
		}
	})
};

// Login Request

module.exports.loginUser = (pmBody) => {
	return User.findOne({
		email: pmBody.email,
		password: pmBody.password
	}).then(result => {
		if(result == null) {
			return false
		} else {
			 const isPasswordCorrect = bcrypt.compare(pmBody.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

// Check Duplicate Email

module.exports.checkDupEmail = (pmBody) => {
	return User.find({email: pmBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

// Retrieve User Info

module.exports.getProfile = (pmBody) => {
	return User.findById(pmBody.id).then(result => {
		if(result == null) {
			return false
		} else {
			result.password = "*****"
		}
		return result
	})
};

// Add an Order

module.exports.addOrder = async (data) => {
	let isUserValid = await User.findById(data.userId).then(user => {
		user.orderedProduct.push({productId: data.productId, productName: data.productName, quantity: data.quantity});

		return user.save().then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	});

	let addProduct = await Product.findById(data.productId).then(cart => {
		cart.userOrder.push({userId: data.userId});
		return cart.save().then((cart, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	});

	if(isUserValid && addProduct) {
		return true
	} else {
		return false
	}
}

// Get All User (admin only)

module.exports.getAllUser = (pmbody) => {
	return User.find().then(result => {
		return result
	})
}