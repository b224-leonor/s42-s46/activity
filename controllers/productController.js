const User = require('../models/User');
const Product = require("../models/Product");
const bcrypt = require("bcrypt");

// Add Product

module.exports.addProduct = (pmBody) => {

	let newProduct = new Product ({
		name: pmBody.name,
		description: pmBody.description,
		price: pmBody.price
	});

	return newProduct.save().then((product, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};

// Get ALL Product

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result
	})
};

// Get All Products (Admin Only)

module.exports.allProducts = () => {
	return Product.find({}).then(result => {
		return result
	})
};

// Get ONE Product

module.exports.getOneProduct = (parameter) => {
	return Product.findById(parameter.productId).then(result => {
		return result
	})
};

// Update a product

module.exports.updateProduct = (parameter, pmBody) => {
	let updatedProduct = {
		name: pmBody.name,
		description: pmBody.description,
		price: pmBody.price
	}
	return Product.findByIdAndUpdate(parameter.productId, updatedProduct).then((updatedProduct, error) => {
		if(error) {
			return false
		} else {
			return updatedProduct
		}
	})
};

// Archive a product

module.exports.archiveProduct = (parameter, pmBody) => {
	let noStock = {
		isActive: pmBody.isActive
	}
	return Product.findByIdAndUpdate(parameter.productId, noStock).then((noStock, error) => {
		if(error) {
			return false
		} else {
			return noStock
		}
	})
};

