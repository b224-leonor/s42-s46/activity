const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
mongoose.set("strictQuery", false);


const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://leonor_224:admin123@224-leonor.xr9pdhl.mongodb.net/prototype-API?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", () => console.error("Connection error."));
db.once("open", () => console.log("Connected to MongoDB."));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(port, () => {console.log(`API is now running at port ${port}`)});