const express = require("express");
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");
const Product = require("../models/Product");

// Register a User

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Login Request

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Check duplicate email

router.post("/checkEmail", (req, res) => {
	userController.checkDupEmail(req.body).then(resultFromController => res.send(resultFromController))
});

// Retrieve User Info

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Add an order

router.post("/orders", auth.verify,  (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		
		productId : req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		userId: userData.id
	}
	
	if(userData.isAdmin){
		res.send("You are an admin")
	} else {

	userController.addOrder(data).then(resultFromController => res.send(resultFromController))
	}
});

router.get("/", auth.verify, (req, res) => {
	
	userController.getAllUser().then(resultFromController => res.send(resultFromController))
})

module.exports = router;