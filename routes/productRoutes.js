const express = require("express");
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");

// Add product

router.post("/addProduct", (req, res) => {

	const adminUser = auth.decode(req.headers.authorization)

	if(adminUser.isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});

// Get ALL Products

router.get("/", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// Get all products (Admin only)

router.get("/allProducts", (req, res) => {
	const adminUser = auth.decode(req.headers.authorization)

	if(adminUser.isAdmin){
		productController.allProducts().then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Not an admin")
	}
});

// Get ONE Product

router.get("/:productId", (req, res) => {
	productController.getOneProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Update product

router.put("/:productId", (req, res) => {
	const adminUser = auth.decode(req.headers.authorization)

	if(adminUser.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Not an admin")
	}
});

// Archive a product

router.put("/archive/:productId", (req, res) => {
	const adminUser = auth.decode(req.headers.authorization)

	if(adminUser.isAdmin){
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Not an admin")
	}
});

module.exports = router;