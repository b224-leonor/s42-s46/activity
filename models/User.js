const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;
const Product = require("../models/Product");
const User = require('../models/User');

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProduct: [
			{
				productId: {
					type: String,
					required: [true, "productId is required"]
				},
				productName: String,
				quantity: Number,
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
});

module.exports = mongoose.model("User", userSchema);