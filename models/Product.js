const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;
const User = require('../models/User');

const productSchema = new mongoose.Schema({
	name: String,
	description: String,
	price: Number,
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	userOrder: [{
		userId: {
			type: ObjectId,
			ref: "User"
		},
		orderId: String
	}]
});

module.exports = mongoose.model("Product", productSchema);